import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';

//Router-dom
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

//PAGES
import GalleryPage from './pages/GalleryPage/GalleyPage';
import HomePage from './pages/HomePage/HomePage';
import GestionPage from './pages/GestionPage/GestionPage';
import CartPage from './pages/Cart/CartPage';
import CallToAPI from './components/CallToAPI/CallToAPI';
import UseReducer from './components/UseReducer/UseReducer';

//HOOKS
import React, { useEffect,  useState } from 'react';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import { NotFound } from './components/NotFound/NotFound';

export const apiHook = React.createContext();

function App() {

  //API

  const [listApi, setlistApi] = useState([]);

  const listApiFn = async () => {
    const res = await CallToAPI()
    setlistApi(res.data.apartments)
    console.log(res.data.apartments)
  }

  useEffect(() => {listApiFn()}, []);

  //CART
  const [cart, setCart] = useState([]);

  return (
    <div className="app">
      <apiHook.Provider value={listApi}>
        <BrowserRouter>
          <Header></Header>
            <Routes>
            <Route path='*' element={<NotFound />} />
              <Route path="/">
                <Route index element={<HomePage  />} />
                <Route path="gallery">
                  <Route index element={< GalleryPage setCart={setCart} listApi={listApi} />} />
                  <Route path=":element" element={<UseReducer></UseReducer>}/>
                </Route>
                <Route path="gestion">
                  <Route index element={< GestionPage listApi={listApi} setlistApi={setlistApi}/>} />
                </Route>
                <Route path="cart">
                  <Route index element={< CartPage cart={cart} setCart = {setCart}/>} />
                </Route>
              </Route>
            </Routes>
          <Footer></Footer>
        </BrowserRouter>
      </apiHook.Provider>
    </div>
  );
}

export default App;
