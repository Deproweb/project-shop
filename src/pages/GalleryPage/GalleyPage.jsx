import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Gallery from "../../components/Gallery/Gallery";

export default function GalleryPage ({setCart, cart}) {

    return (
    <div>
        <Gallery setCart={setCart}></Gallery>
    </div>)

} 