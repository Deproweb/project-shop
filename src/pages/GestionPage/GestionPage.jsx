import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Form from "../../components/Form/Form";

export default function GestionPage({setlistApi, listApi}) {

    return (
    <div>
        <Form setlistApi={setlistApi} listApi={listApi}></Form>
    </div>)

}