import Header from "../../components/Header/Header";
import './CartPage.scss'

//Boostrap
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Footer from "../../components/Footer/Footer";


export default function CartPage ({cart, setCart}) {

    const deleteItem = (i) => {
        const newList = [...cart];
        newList.splice(i, 1);
        setCart(newList);
    }

    return (
        <>
            { cart.length > 0 && <div>
                <div className="c-cart__full">
                    <h4>Estos son los apartamentos que tienes el carrito.</h4>
                    <h5>Sigue el proceso de registro y pago en nuestra plataforma</h5>
                </div>
            {cart.map((element, index) => 
                <div key={element.id} className="c-cart">
                    <Card className="c-cart__card" bg={"danger"} style={{ width: '18rem' }}>
                        <Card.Img variant="top" src={element.imagen} />
                        <Card.Body>
                            <Card.Title>Apartamento en {element.ciudad}</Card.Title>
                            <Card.Subtitle>Id del apartamento: {element.id}</Card.Subtitle>
                            <Button className="c-cart__card__button" onClick={()=>{deleteItem(index)}} variant="light">Eliminar</Button>
                        </Card.Body>
                        </Card>
                </div>
            )}
            </div>}
            { cart.length === 0 && 
                <div className="c-cart__empty">
                    <h4>Todavía no hay nada en el carrito</h4>
                    <h5>Echa un vistazo a nuestras ofertas y elige una de ellas</h5>
                </div>} 
        </>
    )

}