import { useReducer } from "react";

const UseReducer = () => {

    const reducer = (state, action) => {

        switch (action.type) {

            case 'CHANGE_NAME':
                
                return {
                    ...state,
                    name: action.payload
                }
        
            case 'CHANGE_EMAIL':
                
            return {
                ...state,
                email: action.payload
            }

            case 'CHANGE_AGE':
                
            return {
                ...state,
                age: action.payload
            }

            default:
                return state
        }

    }
    
    const initialState = {
        name: '',
        email: '',
        age: 0
    }

    const [state, dispatch] = useReducer(reducer, initialState);

    const handleInputName = (ev) => {

        dispatch ({type: 'CHANGE_NAME', payload: ev.target.value})

    };

    const handleInputEmail = (ev) => {

        dispatch ({type: 'CHANGE_EMAIL', payload: ev.target.value})

    };

    const handleInputAge = (ev) => {

        dispatch ({type: 'CHANGE_AGE', payload: ev.target.value})

    };

    return <div>
        <h2>Prueba UseReducer</h2>
        <form action="">
                <div>
                    <label htmlFor="name">Name</label>
                    <input  id="name" type="text" value={state.name} onChange={handleInputName}  />
                </div>
                <div> 
                    <label htmlFor="email">Email</label>
                    <input  id="email" type="text" value={state.email} onChange={handleInputEmail}  />
                </div>
                <div> 
                    <label htmlFor="age">Age</label>
                    <input  id="age" type="number" value={state.age} onChange={handleInputAge}  />
                </div>
                <button>Click</button>
        </form>
        <h3>{state.name}</h3>
        <h3>{state.email}</h3>
        <h3>{state.age}</h3>
    </div>;
};

export default UseReducer;

