
import React from 'react';
import './Header.scss'

//Router-Dom
import { Link } from "react-router-dom";

//Boostrap
import Nav from 'react-bootstrap/Nav'

export default function Header() {
    return (
        <div className="header">
            <div className="header__logo">
                Upgrade Vacation
            </div>
                <Nav className="justify-content-center header__nav" activeKey="/home">
                    <Nav.Item>
                        <Link to="/" className="header__nav__link">Home</Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/gallery" className="header__nav__link">Gallery</Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/gestion" className="header__nav__link">Gestion</Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/cart" className="header__nav__link">Cart</Link> 
                    </Nav.Item>
                </Nav>
        </div>
    );
}

