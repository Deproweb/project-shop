
import React, { useContext } from "react";
import { useForm } from "react-hook-form";
import { apiHook } from "../../App";

import './Form.scss'

export default function Form({setlistApi}) {

    const list = useContext(apiHook)

    const { register, handleSubmit, formState: { errors } } = useForm();
    
    const onSubmit = (data, ev) => {
        const listForm = [...list];
        listForm.unshift(data)
        ev.target.reset(); 
        setlistApi(listForm)
    };

    return (

    <form className='c-form' onSubmit={handleSubmit(onSubmit)}>

        <h5 className='c-form__title'>Registra tu apartamento con nosotros</h5>

        <input className='c-form__input' placeholder="Ciudad" {...register("ciudad" , {required: true })} />
        {errors.ciudad && <span>Este apartado es obligatorio</span>}

        <input className='c-form__input' placeholder="Id" {...register("id", { required: true })} />
        {errors.Id && <span>Este apartado es obligatorio</span>}
        
        <input className='c-form__input' placeholder="Imagen" {...register("imagen", { required: true })} />
        {errors.Id && <span>Este apartado es obligatorio</span>}

        <input className='c-form__button' type="submit" />
    </form>
    
);
}