import Carousel from 'react-bootstrap/Carousel'

export default function Carrousel() {
    return (
    <div>
        <Carousel>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://cdn.pixabay.com/photo/2016/04/25/23/30/house-1353389_1280.jpg"
                    alt="First slide"
                />
            <Carousel.Caption>
                <h3>Encuentra el apartamento que quieres</h3>
                <p>Los mejores destinos</p>
            </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://cdn.pixabay.com/photo/2016/11/21/15/09/apartments-1845884_1280.jpg"
                    alt="Second slide"
                />
            <Carousel.Caption>
                <h3>Desde Madrid a Nueva York</h3>
                <p>La cadena elegida por el 89% de los clientes</p>
            </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://cdn.pixabay.com/photo/2021/02/02/18/46/city-5974876_1280.jpg"
                    alt="Third slide"
                />
                <Carousel.Caption>
                <h3>Disfruta de nuestras ofertas</h3>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    </div>)
};
