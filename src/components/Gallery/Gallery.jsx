import {useContext, useState } from "react";
import "./Gallery.scss"

//Bootstrap
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import { apiHook } from "../../App";


export default function Gallery ({setCart, listApi}) {

    //API

    const list = useContext(apiHook)

    //FINDER

    const [inputValue, setInputValue] = useState('');
    
    const [showFinder, setShowFinder] = useState(false);

    const [cityFound, setCityFound] = useState({});

    const inputValueFn = (ev) => {
        setInputValue(ev.target.value)
    }

    const finderFn = () => {

        list.forEach(element => {
            if (element.ciudad === inputValue) {
                setShowFinder(true)
                setCityFound(element)
            }
        });
    }

    //CART
    const [cityCart] = useState([]);

    const toCartFn = (apartment) => {
        cityCart.unshift(apartment)
        setCart(cityCart)
        console.log(cityCart)
    }

    return (
        <>
        <div className="c-finder">
            <label className="c-finder__label" htmlFor="finder">Filtra por ciudad</label>
            <input className="c-finder__input" type="text" value={inputValue} onChange={inputValueFn} />
            <Button className="c-finder__button" onClick={finderFn} variant="primary">Filtrar</Button>
        </div>
        { showFinder && <div className="c-finder__showed">
            <Card bg="light" className="c-gallery__card" key={cityFound.ciudad} style={{ width: '18rem' }}>
                    <Card.Img className="c-gallery__card__img" variant="top" src={cityFound.imagen} />
                    <Card.Body>
                    <Card.Title>Apartamento en {cityFound.ciudad}</Card.Title>
                    <Card.Subtitle>ID {cityFound.id}</Card.Subtitle>    
                    <Card.Text>
                        Este apartamento ofrece ...
                    </Card.Text>
                    <Button onClick={()=>{toCartFn(cityFound)}} variant="primary">Reservar</Button>
                    </Card.Body>
                </Card>
        </div>}
        <div className="c-gallery">
            {list.map(element => 
                <Card  className="c-gallery__card" key={element.ciudad} style={{ width: '18rem' }}>
                    <Card.Img className="c-gallery__card__img" variant="top" src={element.imagen} />
                    <Card.Body>
                    <Card.Title>Apartamento en {element.ciudad}</Card.Title>
                    <Card.Subtitle>ID {element.id}</Card.Subtitle>    
                    <Card.Text>
                        Este apartamento ofrece ...
                    </Card.Text>
                    <Button onClick={()=>{toCartFn(element)}} variant="primary">Reservar</Button>
                    </Card.Body>
                </Card>
            )}
        </div>
        </>
    )

}